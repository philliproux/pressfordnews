﻿using NUnit.Framework;
using Pressford.News.DomainModel.Entities;

namespace Pressford.News.DomainModal.Tests.Entities
{
    public class ApplicationUserTests
    {
        [Test]
        public void DisplayNameIsInCorrectFormat()
        {
            var sut = new ApplicationUser
            {
                FirstName = "John", 
                LastName = "Phillips"
            };
            Assert.That(sut.DisplayName, Is.EqualTo("John Phillips"));
        }
    }
}
