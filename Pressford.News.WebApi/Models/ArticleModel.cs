﻿using System.ComponentModel.DataAnnotations;

namespace Pressford.News.WebApi.Models
{
    public class ArticleModel
    {
        public int ArticleId { get; set; }
        [Required]
        [MinLength(4)]
        public string Title { get; set; }
        [Required]
        [MinLength(20)]
        public string Body { get; set; }
        [Required]
        [MinLength(4)]
        public string Author { get; set; }
    }
}