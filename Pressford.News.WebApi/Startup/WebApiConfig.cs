﻿using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Pressford.News.WebApi.Controllers;
using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace Pressford.News.WebApi.Startup
{
    public class WebApiConfig
    {
        public static void Configure(IAppBuilder app, IContainer iocContainer, HttpConfiguration config)
        {
            RegisterRoutes(config);
            RegisterDependencies(iocContainer);
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(iocContainer); ;
            app.UseAutofacMiddleware(iocContainer);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }

        public static void RegisterDependencies(IContainer iocContainer)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof(ArticlesController).Assembly);
            builder.Update(iocContainer);
        }

        public static void RegisterRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
