﻿using Pressford.News.Business.Services;
using Pressford.News.DomainModel.Entities;
using Pressford.News.WebApi.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Pressford.News.DomainModel.DTOs;
using Article = Pressford.News.Business.Contract.DTOs.Article;

namespace Pressford.News.WebApi.Controllers
{
    public class ArticlesController : ApiController
    {
        private readonly IArticleService _articleService;

        public ArticlesController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [ResponseType(typeof(Article))]
        public IHttpActionResult Get()
        {
            var articles = _articleService.GetArticles(User);
            return Ok(articles);
        }

        [Authorize(Roles = UserRoles.Publisher)]
        public HttpResponseMessage Post([FromBody]ArticleModel article)
        {
            if (ModelState.IsValid)
            {
                _articleService.Add(article.Title, article.Body, article.Author, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPost]
        [Route("api/articles/like/{id}")]
        [Authorize(Roles = UserRoles.Employee)]
        public IHttpActionResult Like(int id)
        {
            // TODO: Validate not too many likes etc (CommandValidator)
            _articleService.LikeArticle(User, id);
            return Ok();
        }

        [HttpGet]
        [ResponseType(typeof(ArticleLiked))]
        [Route("api/articles/like/ByDate/{id}")]
        public IHttpActionResult LikesByArticleId(int id)
        {
            var likes = _articleService.GetLikesForLastThirtyDaysGroupedByArticleId(id);
            return Ok(likes);
        }
    }
}
