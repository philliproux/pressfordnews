﻿using System.Collections.Generic;
using Pressford.News.DomainModel.DTOs;
using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data.Repositories
{
    public interface IArticleRepository : IGenericRepository<Article>
    {
        IEnumerable<Article> GetAllSortedByLatestPublishDate();
        IEnumerable<Article> GetByUsernameSortedByLatestPublishDate(string username);
        IEnumerable<ArticleLiked> GetLikesForLastThirtyDaysGroupedByArticleId(int articleId);
    }
}