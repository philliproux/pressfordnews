using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data.Repositories
{
    public interface ILikeRepository : IGenericRepository<Like>
    {

    }
}