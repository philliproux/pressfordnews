﻿using System.Linq;
using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data.Repositories
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(PressfordNewsDbContext db)
            : base(db)
        {

        }

        public string GetDisplayName(string username)
        {
            var user = AsQueryable()
                    .Where(x => x.UserName == username)
                    .Select(x => new { x.FirstName, x.LastName })
                    .Single();
            return string.Format("{0} {1}", user.FirstName, user.LastName);
        }

        public ApplicationUser GetByUsername(string username)
        {
            return AsQueryable().FirstOrDefault(x => x.UserName == username);
        }
    }
}