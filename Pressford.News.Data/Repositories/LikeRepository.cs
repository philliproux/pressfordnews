﻿using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data.Repositories
{
    public class LikeRepository : GenericRepository<Like>, ILikeRepository
    {
        public LikeRepository(PressfordNewsDbContext db) : base(db)
        {

        }
    }
}