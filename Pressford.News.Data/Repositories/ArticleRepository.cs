﻿using Pressford.News.DomainModel.DTOs;
using Pressford.News.DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Pressford.News.Data.Repositories
{
    public class ArticleRepository : GenericRepository<Article>, IArticleRepository
    {
        public ArticleRepository(PressfordNewsDbContext db) : base(db)
        {

        }

        public IEnumerable<Article> GetAllSortedByLatestPublishDate()
        {
            return AsQueryable()
                .OrderByDescending(article => article.PublishDate)
                .ToList();
        }

        public IEnumerable<Article> GetByUsernameSortedByLatestPublishDate(string username)
        {
            return AsQueryable()
                .Where(x => x.Publisher.UserName == username)
                .OrderByDescending(article => article.PublishDate)
                .ToList();
        }

        public IEnumerable<ArticleLiked> GetLikesForLastThirtyDaysGroupedByArticleId(int articleId)
        {
            var lastThirtyDays = DateTime.Now.AddDays(-30);
            var likes = AsQueryable()
                .Where(x => x.ArticleId == articleId)
                .SelectMany(x => x.Likes)
                .Where(y => y.LikedAtDateTime >= lastThirtyDays)
                .GroupBy(x => DbFunctions.TruncateTime(x.LikedAtDateTime))
                .OrderBy(x => x.Key)
                .Select(x => new
                {
                    Count = x.Count(),
                    Date = (DateTime) x.Key
                })
                .ToList()
                .Select(x => new ArticleLiked()
                {
                    Count = x.Count,
                    Date = x.Date.ToString("d MMM")
                })
                .ToList();
            return likes;
        }
    }
}