﻿using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data.Repositories
{
    public interface IUserRepository : IGenericRepository<ApplicationUser>
    {
        string GetDisplayName(string username);
        ApplicationUser GetByUsername(string username);
    }
}