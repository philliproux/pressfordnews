namespace Pressford.News.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Article_Like_Comment_Entities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        LikeId = c.Int(nullable: false, identity: true),
                        ArticleId = c.Int(nullable: false),
                        EmployeeId = c.String(maxLength: 128),
                        LikedAtDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.LikeId)
                .ForeignKey("dbo.Articles", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId)
                .Index(t => t.ArticleId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ArticleId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                        PublisherId = c.String(maxLength: 128),
                        Author = c.String(),
                    })
                .PrimaryKey(t => t.ArticleId)
                .ForeignKey("dbo.AspNetUsers", t => t.PublisherId)
                .Index(t => t.PublisherId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        DateCommented = c.DateTime(nullable: false),
                        EmployeeId = c.String(maxLength: 128),
                        Article_ArticleId = c.Int(),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.AspNetUsers", t => t.EmployeeId)
                .ForeignKey("dbo.Articles", t => t.Article_ArticleId)
                .Index(t => t.EmployeeId)
                .Index(t => t.Article_ArticleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Likes", "EmployeeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Likes", "ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Articles", "PublisherId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "Article_ArticleId", "dbo.Articles");
            DropForeignKey("dbo.Comments", "EmployeeId", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "Article_ArticleId" });
            DropIndex("dbo.Comments", new[] { "EmployeeId" });
            DropIndex("dbo.Articles", new[] { "PublisherId" });
            DropIndex("dbo.Likes", new[] { "EmployeeId" });
            DropIndex("dbo.Likes", new[] { "ArticleId" });
            DropTable("dbo.Comments");
            DropTable("dbo.Articles");
            DropTable("dbo.Likes");
        }
    }
}
