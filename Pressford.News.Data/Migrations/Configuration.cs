using Pressford.News.DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Pressford.News.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PressfordNewsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PressfordNewsDbContext context)
        {
            SeedUsersAndRoles(context);
            SeedArticles(context);
        }

        static string UserAdminId = "94b80b9c-023f-4110-9203-3f4e03b5bdb0";
        static string UserEmployeeJohnId = "3386cd21-12c2-411b-b895-6c24d1fbf71b";
        static string UserEmployeeLauraId = "c395529f-e10e-45ed-ac82-503cc7fc6d6f";
        static string UserPublisherAbc = "19b3e46e-5a60-48cb-8035-4411b75dc5f9";
        static string UserPublisherSbs = "407fa846-ed8d-4b43-819b-26b29d28e179";


        #region UsersAndRoles
        internal void SeedUsersAndRoles(PressfordNewsDbContext context)
        {
            if (context.Roles.Any(x => x.Name == UserRoles.Administrator))
                return;
            var identityManager = new IdentityManager(context);
            identityManager.CreateRole(UserRoles.Administrator);
            identityManager.CreateRole(UserRoles.Publisher);
            identityManager.CreateRole(UserRoles.Employee);

            SeedAdministratorAccount(identityManager);
            SeedEmployeeJohn(identityManager);
            SeedEmployeeLaura(identityManager);
            SeedPublisherAbc(identityManager);
            SeedPublisherSBS(identityManager);
        }

        private void SeedAdministratorAccount(IdentityManager identityManager)
        {
            var adminUser = new ApplicationUser()
            {
                Id = UserAdminId,
                FirstName = "William",
                LastName = "Pressford",
                UserName = "william@pressfordconsulting.co.uk",
                Email = "william@pressfordconsulting.co.uk",
                EmailConfirmed = true,
            };

            var success = identityManager.CreateUser(adminUser, "P@ssword123");
            if (!success) return;

            identityManager.AddUserToRole(adminUser.Id, UserRoles.Administrator);
        }

        private void SeedEmployeeJohn(IdentityManager identityManager)
        {
            var employee = new ApplicationUser()
            {
                Id = UserEmployeeJohnId,
                FirstName = "John",
                LastName = "Buchanan",
                UserName = "john@pressfordconsulting.co.uk",
                Email = "john@pressfordconsulting.co.uk",
                EmailConfirmed = true,
            };

            var success = identityManager.CreateUser(employee, "P@ssword123");
            if (!success) return;

            identityManager.AddUserToRole(employee.Id, UserRoles.Employee);
        }

        private void SeedEmployeeLaura(IdentityManager identityManager)
        {
            var employee = new ApplicationUser()
            {
                Id = UserEmployeeLauraId,
                FirstName = "Laura",
                LastName = "James",
                UserName = "laura@pressfordconsulting.co.uk",
                Email = "laura@pressfordconsulting.co.uk",
                EmailConfirmed = true,
            };

            var success = identityManager.CreateUser(employee, "P@ssword123");
            if (!success) return;

            identityManager.AddUserToRole(employee.Id, UserRoles.Employee);
        }

        private void SeedPublisherAbc(IdentityManager identityManager)
        {
            var publisher = new ApplicationUser()
            {
                Id = UserPublisherAbc,
                FirstName = "Ricky",
                LastName = "Anderson",
                UserName = "ricky@abcpublishers.co.uk",
                Email = "ricky@abcpublishers.co.uk",
                EmailConfirmed = true,
            };

            var success = identityManager.CreateUser(publisher, "P@ssword123");
            if (!success) return;

            identityManager.AddUserToRole(publisher.Id, UserRoles.Publisher);
        }

        private void SeedPublisherSBS(IdentityManager identityManager)
        {
            var publisher = new ApplicationUser()
            {
                Id = UserPublisherSbs,
                FirstName = "Jimmy",
                LastName = "Johns",
                UserName = "jimmy@sbspublishers.co.uk",
                Email = "jimmy@sbspublishers.co.uk",
                EmailConfirmed = true,
            };

            var success = identityManager.CreateUser(publisher, "P@ssword123");
            if (!success) return;

            identityManager.AddUserToRole(publisher.Id, UserRoles.Publisher);
        }
        #endregion

        #region Articles
        internal void SeedArticles(PressfordNewsDbContext context)
        {
            var rnd = new Random();

            for (var articleId = 1; articleId <= 50; articleId++)
            {
                var numberOfLikes = rnd.Next(40, 100);
                var likes = new List<Like>();
                for (var i = 1; i <= numberOfLikes; i++)
                {
                    var daysAgo = rnd.Next(2, 30);
                    likes.Add(new Like()
                    {
                        EmployeeId = UserEmployeeJohnId,
                        LikedAtDateTime = DateTime.Now.AddDays(daysAgo)
                    });
                }
                context.Articles.AddOrUpdate(x => x.ArticleId, new Article()
                {
                    ArticleId = articleId,
                    Author = "Dan Smith",
                    PublishDate = DateTime.Now.AddDays(-23).AddHours(-20),
                    PublisherId = articleId % 2 == 0 ? UserPublisherAbc : UserPublisherSbs,
                    Title = "Article " + articleId,
                    Body = "This is an article body",
                    Comments = new List<Comment>()
                    {
                        new Comment()
                        {
                            EmployeeId = UserEmployeeJohnId,
                            DateCommented = DateTime.Now.AddDays(-15).AddHours(-15),
                            Body = "Comment 1",
                        },
                        new Comment()
                        {
                            EmployeeId = UserEmployeeLauraId,
                            DateCommented = DateTime.Now.AddDays(-17).AddHours(-13),
                            Body = "Comment 2",
                        }
                    },
                    Likes = likes
                });
            }
        }
        #endregion
    }
}
