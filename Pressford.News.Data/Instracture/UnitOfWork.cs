﻿using System;
using Pressford.News.Data.Repositories;

namespace Pressford.News.Data.Instracture
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly PressfordNewsDbContext _context;

        private bool _disposed = false;

        public UnitOfWork(PressfordNewsDbContext context)
        {
            _context = context;
        }


        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
