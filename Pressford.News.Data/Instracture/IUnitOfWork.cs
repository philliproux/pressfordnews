﻿namespace Pressford.News.Data.Instracture
{
    public interface IUnitOfWork
    {
        void Save();
        void Dispose();
    }
}