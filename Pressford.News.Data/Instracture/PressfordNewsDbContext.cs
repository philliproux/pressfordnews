﻿using Microsoft.AspNet.Identity.EntityFramework;
using Pressford.News.DomainModel.Entities;
using System.Data.Entity;

namespace Pressford.News.Data
{
    public class PressfordNewsDbContext : IdentityDbContext<ApplicationUser>
    {
        public PressfordNewsDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public static PressfordNewsDbContext Create()
        {
            return new PressfordNewsDbContext();
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Like> ArticleLikes { get; set; }
    }
}