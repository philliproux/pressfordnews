﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pressford.News.DomainModel.Entities;

namespace Pressford.News.Data
{
    public class IdentityManager
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public IdentityManager(PressfordNewsDbContext db)
        {
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        public bool RoleExists(string name)
        {
            return _roleManager.RoleExists(name);
        }

        public bool CreateRole(string name)
        {
            var idResult = _roleManager.Create(new IdentityRole(name));
            return idResult.Succeeded;
        }

        public bool CreateUser(ApplicationUser user, string password)
        {
            var idResult = _userManager.Create(user, password);
            return idResult.Succeeded;
        }

        public bool CreateUser(ApplicationUser user)
        {
            var idResult = _userManager.Create(user);
            return idResult.Succeeded;
        }

        public bool AddUserToRole(string userId, string roleName)
        {
            var idResult = _userManager.AddToRole(userId, roleName);
            return idResult.Succeeded;
        }

        public bool ChangePassword(string userId, string oldPassword, string newPassword)
        {
            var result = _userManager.ChangePassword(userId, oldPassword, newPassword);
            return result.Succeeded;
        }
    }
}