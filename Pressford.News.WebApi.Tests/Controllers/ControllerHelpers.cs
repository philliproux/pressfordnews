using System;
using System.Linq;
using System.Web.Http;

namespace Pressford.News.WebApi.Tests.Controllers
{
    public class ControllerHelpers
    {
        public static bool WebApiActionHasRoles(ApiController controller, string action, string roles)
        {
            var controllerType = controller.GetType();
            var method = controllerType.GetMethod(action);

            object[] filters = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            if (!filters.Any())
            {
                throw new Exception();
            }

            var rolesOnCurrentMethodsAttribute = filters.SelectMany(attrib => ((AuthorizeAttribute)attrib).Roles.Split(new[] { ',' })).ToList();
            var rolesToCheckAgainst = roles.Split(',').ToList();

            return !rolesOnCurrentMethodsAttribute.Except(rolesToCheckAgainst).Any() && !rolesToCheckAgainst.Except(rolesOnCurrentMethodsAttribute).Any();
        }
    }
}