﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Business.Contract.DTOs;
using Pressford.News.Business.Services;
using Pressford.News.WebApi.Controllers;
using System.Collections.Generic;
using System.Web.Http.Results;

namespace Pressford.News.WebApi.Tests.Controllers
{
    public class ArticlesControllerTests
    {
        [Test]
        public void OnlyPublisherRoleCanAddArticleViaApiEndPoint()
        {
            var sut = new ArticlesController(null);
            Assert.IsTrue(ControllerHelpers.WebApiActionHasRoles(sut, "Post", "Publisher"));
            Assert.IsFalse(ControllerHelpers.WebApiActionHasRoles(sut, "Post", "Administrator"));
            Assert.IsFalse(ControllerHelpers.WebApiActionHasRoles(sut, "Post", "Employee"));
        }

        [Test]
        public void GetRequestReturnsCorrectStatusCode()
        {
            var sut = new ArticlesController(A.Fake<IArticleService>());
            var actionResult = sut.Get();
            Assert.IsInstanceOf<OkNegotiatedContentResult<IEnumerable<Article>>>(actionResult);
        }
    }
}
