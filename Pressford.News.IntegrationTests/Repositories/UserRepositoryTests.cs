﻿using NUnit.Framework;
using Pressford.News.Data;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using Pressford.News.IntegrationTests.SampleData;
using System.Data.Entity;

namespace Pressford.News.IntegrationTests.Repositories
{
    [TestFixture]
    public class UserRepositoryTests
    {
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            Database.SetInitializer(new DatabaseTestInitializer());
        }

        [Test]
        public void UserDisplayNameIsInCorrectFormat()
        {
            var dbContext = new PressfordNewsDbContext();
            var userRepository = new UserRepository(dbContext);
            using (var unitOfWork = new UnitOfWork(dbContext))
            {
                userRepository.Insert(Users.CreateUserWilliam());
                unitOfWork.Save();
                var userDisplayName = userRepository.GetDisplayName("william@pressfordconsulting.co.uk");
                Assert.AreEqual("William Pressford", userDisplayName);
            }
        }
    }
}
