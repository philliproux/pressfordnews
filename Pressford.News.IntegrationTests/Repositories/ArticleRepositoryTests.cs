using NUnit.Framework;
using Pressford.News.Data;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using Pressford.News.IntegrationTests.SampleData;
using System;
using System.Data.Entity;
using System.Linq;

namespace Pressford.News.IntegrationTests.Repositories
{
    [TestFixture]
    public class ArticleRepositoryTests
    {
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            Database.SetInitializer(new DatabaseTestInitializer());
        }

        [Test]
        public void GetAllSortedByLatestPublishDateYieldsResultsOrderedByPublishDateDescending()
        {
            var dbContext = new PressfordNewsDbContext();
            var userRepository = new UserRepository(dbContext);
            var articleRepository = new ArticleRepository(dbContext);
            using (var unitOfWork = new UnitOfWork(dbContext))
            {
                var userWilliam = Users.CreateUserWilliam();
                userRepository.Insert(userWilliam);

                var publishDateOldest = DateTime.Now.AddDays(-5);
                var articleOldest = Articles.CreateArticle(userWilliam, publishDateOldest);
                articleRepository.Insert(articleOldest);

                var publishDateNewest = DateTime.Now.AddDays(-1);
                var articleNewest = Articles.CreateArticle(userWilliam, publishDateNewest);
                articleRepository.Insert(articleNewest);

                var publishDateSecondOldest = DateTime.Now.AddDays(-2);
                var articleSecondOldest = Articles.CreateArticle(userWilliam, publishDateSecondOldest);
                articleRepository.Insert(articleSecondOldest);
                unitOfWork.Save();

                var articles = articleRepository.GetAllSortedByLatestPublishDate().ToArray();
                Assert.AreEqual(publishDateNewest, articles[0].PublishDate);
                Assert.AreEqual(publishDateSecondOldest, articles[1].PublishDate);
                Assert.AreEqual(publishDateOldest, articles[2].PublishDate);
            }
        }
    }
}