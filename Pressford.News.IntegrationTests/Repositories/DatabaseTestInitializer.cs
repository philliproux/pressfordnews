using Pressford.News.Data;

namespace Pressford.News.IntegrationTests.Repositories
{
    public class DatabaseTestInitializer : System.Data.Entity.DropCreateDatabaseAlways<PressfordNewsDbContext>
    {
        protected override void Seed(PressfordNewsDbContext context)
        {

        }
    }
}