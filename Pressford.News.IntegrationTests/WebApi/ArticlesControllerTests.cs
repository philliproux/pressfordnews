﻿using NUnit.Framework;
using System.Net;
using System.Net.Http;

namespace Pressford.News.IntegrationTests.WebApi
{
    public class ArticlesControllerTests
    {
        [Test]
        public void PostReturnsUnauthorisedStatusCodeForNonPublishers()
        {
            using (var client = HttpClientFactory.Create())
            {
                var json = new
                {
                    Title = "Title",
                    Body = "Body",
                    Author = "Author"
                };
                var postResponse = client.PostAsJsonAsync("api/Articles", json).Result;
                Assert.AreEqual(postResponse.StatusCode, HttpStatusCode.Unauthorized);
            }
        }
    }
}
