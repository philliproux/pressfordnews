﻿using Autofac.Integration.WebApi;
using Pressford.News.WebApi.Startup;
using Pressford.News.WebUI;
using System;
using System.Net.Http;
using System.Web.Http.SelfHost;

namespace Pressford.News.IntegrationTests.WebApi
{
    public class HttpClientFactory
    {
        public static HttpClient Create()
        {
            var baseAddress = new Uri("http://localhost:8765");
            var config = new HttpSelfHostConfiguration(baseAddress);
            WebApiConfig.RegisterRoutes(config);

            /* Setup IOC */
            var container = IoCConfig.SetupDiContainer(config);
            WebApiConfig.RegisterDependencies(container);
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container); ;

            var server = new HttpSelfHostServer(config);
            var client = new HttpClient(server);
            try
            {
                client.BaseAddress = baseAddress;
                return client;
            }
            catch
            {
                client.Dispose();
                throw;
            }
        }
    }
}