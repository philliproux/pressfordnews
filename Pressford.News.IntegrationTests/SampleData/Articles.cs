﻿using System;
using Pressford.News.DomainModel.Entities;

namespace Pressford.News.IntegrationTests.SampleData
{
    public class Articles
    {
        public static Article CreateArticle(ApplicationUser publisher, DateTime publishDate)
        {
            return new Article()
            {
                Title = "Title1",
                Author = "Author1",
                Body = "Body1",
                Publisher = publisher,
                PublishDate = publishDate
            };
        }
    }
}