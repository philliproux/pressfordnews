﻿using Pressford.News.DomainModel.Entities;

namespace Pressford.News.IntegrationTests.SampleData
{
    public class Users
    {
        public static ApplicationUser CreateUserWilliam()
        {
            return new ApplicationUser()
            {
                FirstName = "William",
                LastName = "Pressford",
                UserName = "william@pressfordconsulting.co.uk",
                Email = "william@pressfordconsulting.co.uk",
                EmailConfirmed = true,
            };
        }
    }
}
