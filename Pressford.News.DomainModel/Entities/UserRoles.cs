﻿using System.Collections.Generic;

namespace Pressford.News.DomainModel.Entities
{
    public class UserRoles : List<string>
    {
        public const string Administrator = "Administrator";
        public const string Publisher = "Publisher";
        public const string Employee = "Employee";

        public UserRoles()
            : base()
        {
            Add(Administrator);
            Add(Publisher);
            Add(Employee);
        }
    }
}
