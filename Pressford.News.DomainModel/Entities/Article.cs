﻿using System;
using System.Collections.Generic;

namespace Pressford.News.DomainModel.Entities
{
    public class Article
    {
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime PublishDate { get; set; }
        public string PublisherId { get; set; }
        public virtual ApplicationUser Publisher { get; set; }
        public string Author { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
    }
}
