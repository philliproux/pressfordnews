﻿using System;

namespace Pressford.News.DomainModel.Entities
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Body { get; set; }
        public DateTime DateCommented { get; set; }
        public string EmployeeId { get; set; }
        public virtual ApplicationUser Employee { get; set; }
    }
}