﻿using System;

namespace Pressford.News.DomainModel.Entities
{
    public class Like
    {
        public int LikeId { get; set; }
        public int ArticleId { get; set; }
        public virtual Article Article { get; set; }
        public string EmployeeId { get; set; }
        public virtual ApplicationUser Employee { get; set; }
        public DateTime LikedAtDateTime { get; set; }
    }
}