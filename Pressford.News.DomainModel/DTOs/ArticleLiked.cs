﻿using System;

namespace Pressford.News.DomainModel.DTOs
{
    public class ArticleLiked
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }
}
