﻿using ASP;
using NUnit.Framework;
using RazorGenerator.Testing;

namespace Pressford.News.WebUI.ViewTests.Views
{
    [TestFixture]
    public class MenuBarViewTests
    {
        [Test]
        public void MenuOptionsDisplayedWhenUserLoggedIn()
        {
            var sut = new _Views_Shared__MenuBarPartial_cshtml();
            sut.ViewBag.IsAuthenticated = true;
            var html = sut.RenderAsHtml();
            var menuBarElement = html.GetElementbyId("menuBar");
            Assert.IsNotNull(menuBarElement);
        }

        [Test]
        public void MenuOptionsNotDisplayedWhenUserNotLoggedIn()
        {
            var sut = new _Views_Shared__MenuBarPartial_cshtml();
            sut.ViewBag.IsAuthenticated = false;
            var html = sut.RenderAsHtml();
            var menuBarElement = html.GetElementbyId("menuBar");
            Assert.IsNull(menuBarElement);
        }

        [Test]
        public void UserDisplayNameShownWhenUserLoggedIn()
        {
            var sut = new _Views_Shared__MenuBarPartial_cshtml();
            sut.ViewBag.IsAuthenticated = true;
            sut.ViewBag.UserDisplayName = "William Pressford";
            var html = sut.RenderAsHtml();
            var userAnchorElement = html.GetElementbyId("userDisplayName");
            Assert.AreEqual("William Pressford", userAnchorElement.InnerText);
        }
    }
}
