﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Data.Repositories;
using Pressford.News.WebUI.Filters;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Pressford.News.WebUI.ViewTests.Filters
{
    [TestFixture]
    public class AuthoriseUserAttributeTests
    {
        private AuthorizationContext _authorizationContext;
        private HttpRequestBase _request;
        private HttpContextBase _mockHttpContext;

        [SetUp]
        public void SetUp()
        {
            _request = A.Fake<HttpRequestBase>();
            A.CallTo(() => _request.HttpMethod).Returns("GET");

            var response = A.Fake<HttpResponseBase>();

            _mockHttpContext = A.Fake<HttpContextBase>();
            A.CallTo(() => _mockHttpContext.Request).Returns(_request);
            A.CallTo(() => _mockHttpContext.Response).Returns(response);

            var controllerContext = new ControllerContext(_mockHttpContext, new RouteData(), A.Fake<ControllerBase>());
            _authorizationContext = new AuthorizationContext(controllerContext, A.Fake<ActionDescriptor>());
        }

        [Test]
        public void UnauthenticatedRequestSetsViewBagIsAuthenticatdToFalse()
        {
            var sut = new AuthoriseUserAttribute();
            A.CallTo(() => _request.IsAuthenticated).Returns(false);

            sut.OnAuthorization(_authorizationContext);

            Assert.IsFalse(_authorizationContext.Controller.ViewBag.IsAuthenticated);
        }

        [Test]
        public void AuthenticatedRequestSetsViewBagIsAuthenticatedToTrue()
        {
            var sut = new AuthoriseUserAttribute();
            A.CallTo(() => _request.IsAuthenticated).Returns(true);
            sut.UserRepository = A.Fake<IUserRepository>(); ;

            sut.OnAuthorization(_authorizationContext);

            Assert.IsTrue(_authorizationContext.Controller.ViewBag.IsAuthenticated);
        }

        [Test]
        public void AuthenticatedRequestSetsViewBagDisplayNameToTrue()
        {
            var sut = new AuthoriseUserAttribute();
            A.CallTo(() => _request.IsAuthenticated).Returns(true);
            A.CallTo(() => _mockHttpContext.User.Identity.Name).Returns("william@pressfordconsulting.co.uk");
            var userRepository = A.Fake<IUserRepository>();
            A.CallTo(() => userRepository.GetDisplayName("william@pressfordconsulting.co.uk")).Returns("William Pressford");
            sut.UserRepository = userRepository;
            
            sut.OnAuthorization(_authorizationContext);

            Assert.AreEqual("William Pressford", _authorizationContext.Controller.ViewBag.UserDisplayName);
        }
    }
}
