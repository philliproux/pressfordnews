﻿using NUnit.Framework;
using Pressford.News.WebUI.Controllers;
using Pressford.News.WebUI.Filters;
using System.Linq;
using System.Reflection;

namespace Pressford.News.WebUI.ViewTests.Controllers
{
    public class BaseAuthorisedControllerTests
    {
        [Test]
        public void BaseAuthorisedControllerIsDecoratedWithAuthoriseUserAttribute()
        {
            var controller = new BaseAuthorisedController();
            var type = controller.GetType();
            var attributes = type.GetCustomAttributes((typeof (AuthoriseUserAttribute)));
            Assert.IsTrue(attributes.Any(), "No AuthoriseUserAttribute found on BaseAuthorisedController!");
        }
    }
}
