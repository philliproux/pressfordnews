﻿using Autofac;
using Autofac.Integration.Mvc;
using Pressford.News.Business.Commands;
using Pressford.News.Business.Services;
using Pressford.News.Data;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace Pressford.News.WebUI
{
    public class IoCConfig
    {
        public static IContainer SetupDiContainer(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            #region Setup a common pattern - placed here before RegisterControllers as last one wins
            builder.RegisterType<UnitOfWork>().AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterType<PressfordNewsDbContext>().InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(ArticleRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(ArticleService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(CreateArticleCommand).Assembly)
                .Where(t => t.Name.EndsWith("Handler"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(CreateArticleCommand).Assembly)
                .Where(t => t.Name.EndsWith("QueryHandlerFactory"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            #endregion

            //Register all controllers for the assembly
            // Note that ASP.NET MVC requests controllers by their concrete types, so registering them As<IController>() is incorrect. 
            // Also, if you register controllers manually and choose to specify lifetimes, you must register them as InstancePerDependency() or InstancePerHttpRequest() - 
            // ASP.NET MVC will throw an exception if you try to reuse a controller instance for multiple requests. 
            builder.RegisterControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();
            builder.RegisterFilterProvider();

            // #region Inject HTTP Abstractions
            /*
             The MVC Integration includes an Autofac module that will add HTTP request lifetime scoped registrations for the HTTP abstraction classes. The following abstract classes are included: 
            -- HttpContextBase 
            -- HttpRequestBase 
            -- HttpResponseBase 
            -- HttpServerUtilityBase 
            -- HttpSessionStateBase 
            -- HttpApplicationStateBase 
            -- HttpBrowserCapabilitiesBase 
            -- HttpCachePolicyBase 
            -- VirtualPathProvider 

            To use these abstractions add the AutofacWebTypesModule to the container using the standard RegisterModule method. 
         */
            builder.RegisterModule<AutofacWebTypesModule>();

            var container = builder.Build();

            /* Set the MVC dependency resolver to use Autofac */
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        }
    }
}