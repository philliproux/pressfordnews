﻿using Microsoft.Owin;
using Owin;
using Pressford.News.WebApi.Startup;
using Pressford.News.WebUI;
using System.Web.Http;

[assembly: OwinStartup(typeof(Startup))]
namespace Pressford.News.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var config = new HttpConfiguration();
            var container = IoCConfig.SetupDiContainer(config);
            WebApiConfig.Configure(app, container, config);
        }
    }
}
