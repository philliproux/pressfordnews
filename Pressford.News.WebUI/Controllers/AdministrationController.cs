﻿using System.Web.Mvc;

namespace Pressford.News.WebUI.Controllers
{
    public class AdministrationController : BaseAuthorisedController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}