﻿using Pressford.News.WebUI.Filters;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Pressford.News.WebUI.Controllers
{
    [AuthoriseUser]
    public class BaseAuthorisedController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User != null && User.Identity.GetType() == typeof(ClaimsIdentity))
            {
                var userRole = ((ClaimsIdentity) User.Identity).Claims
                    .Where(c => c.Type == ClaimTypes.Role)
                    .Select(c => c.Value).First();
                ViewBag.UserRole = userRole;
            }
        }
    }
}
