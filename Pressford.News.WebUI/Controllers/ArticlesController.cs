﻿using Pressford.News.DomainModel.Entities;
using System.Web.Mvc;

namespace Pressford.News.WebUI.Controllers
{
    public class ArticlesController : BaseAuthorisedController
    {
        [Authorize(Roles = UserRoles.Publisher)]
        public ActionResult Create()
        {
            return View();
        }
    }
}