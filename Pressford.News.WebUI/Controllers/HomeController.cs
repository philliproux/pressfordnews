﻿using System.Web.Mvc;

namespace Pressford.News.WebUI.Controllers
{
    public class HomeController : BaseAuthorisedController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}