﻿using Pressford.News.Data.Repositories;
using System.Web.Mvc;

namespace Pressford.News.WebUI.Filters
{
    public class AuthoriseUserAttribute : AuthorizeAttribute
    {
        public IUserRepository UserRepository { get; set; } /* Set via AutoFac Property Injection */ 

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Controller.ViewBag.UserDisplayName = UserRepository.GetDisplayName(filterContext.HttpContext.User.Identity.Name);
            }
            filterContext.Controller.ViewBag.IsAuthenticated = filterContext.RequestContext.HttpContext.Request.IsAuthenticated;
        }
    }
}
