﻿(function () {
    angular
        .module("pressfordNewsApp")
        .controller("ViewArticleCtrl", ["likeArticleResource", "article", "$modalInstance", ViewArticleCtrl]);

    function ViewArticleCtrl(likeArticleResource, article, $modalInstance) {
        var vm = this;
        vm.article = article;
        vm.userRole = pressford.userRole;

        vm.like = function () {
            likeArticleResource.save({ id: article.id }, function (data) {
                vm.article.likeCount += 1;
                alert("Article liked!");
            }, function(response) {

            });
        }

        vm.close = function() {
            $modalInstance.close();
        }
    }
}());