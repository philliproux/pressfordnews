﻿(function () {
    angular
        .module("pressfordNewsApp")
        .controller("LikeStatisticsCtrl", ["$http", "article", "$modalInstance", LikeStatisticsCtrl]);

    function LikeStatisticsCtrl($http, article, $modalInstance) {
        var vm = this;
        vm.article = article;

        var url = "api/articles/like/ByDate/" + article.id;
        $http.get(url)
            .then(function (response) {
                var chartData = {
                    labels: [],
                    datasets: [
                        {
                            label: "Likes last 30 days",
                            fillColor: "rgba(151,187,205,0.5)",
                            strokeColor: "rgba(151,187,205,0.8)",
                            highlightFill: "rgba(151,187,205,0.75)",
                            highlightStroke: "rgba(151,187,205,1)",
                            data: []
                        }
                    ]
                };

                for (var key in response.data) {
                    chartData.labels.push(response.data[key].date);
                    chartData.datasets[0].data.push(response.data[key].count);
                }
                var ctx = document.getElementById("statsChart").getContext("2d");
                var barChart = new Chart(ctx).Bar(chartData);
            });

        vm.close = function () {
            $modalInstance.close();
        }
    }
}());