﻿(function() {
    angular
        .module("pressfordNewsApp")
        .controller("CreateArticleCtrl", ["articleResource", CreateArticleCtrl]);

    function CreateArticleCtrl(articleResource) {
        var vm = this;
        vm.article = {};
        vm.message = "";

        this.submit = function() {
            articleResource.save(vm.article,
                function(data) {
                    vm.message = "Successfully saved article";
                },
                function(response) {
                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            vm.message += response.data.modelState[key] + "\r\n";
                        }
                    }
                    if (response.data.exceptionMessage)
                        vm.message += response.data.exceptionMessage;
                });
        }

        this.cancel = function(articleForm) {
            articleForm.$setPristine();
            vm.article = {};
        }
    }
}());