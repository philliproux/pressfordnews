﻿(function () {
    angular
        .module("pressfordNewsApp")
        .controller("ListArticlesCtrl", ["articleResource", "$uibModal", ListArticlesCtrl]);

    function ListArticlesCtrl(articleResource, $uibModal) {
        var vm = this;

        articleResource.query(function (data) {
            vm.articles = data;
        });

        vm.viewArticle = function(article, $event) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "/AngularApp/Articles/viewArticle.html",
                controller: "ViewArticleCtrl as vm",
                size: "lg",
                resolve: {
                    article: function () {
                        return article;
                    }
                }
            });
            $event.preventDefault();
        }

        vm.viewLikeStatistics = function (article, $event) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "/AngularApp/Articles/likeStatisticsView.html",
                controller: "LikeStatisticsCtrl as vm",
                size: "lg",
                resolve: {
                    article: function () {
                        return article;
                    }
                }
            });
            $event.preventDefault();
        }
    }
}());