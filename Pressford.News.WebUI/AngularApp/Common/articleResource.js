﻿(function () { 
    "use strict";
    angular
        .module("pressford.common.services")
        .factory("articleResource", ["$resource", "appsettings", articleResource]);
    
    function articleResource($resource, appsettings) {
        return $resource(appsettings.serverPath + "/api/articles/:id");
    }
}());
