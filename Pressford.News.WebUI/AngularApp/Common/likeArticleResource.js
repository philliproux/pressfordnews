﻿(function () {
    "use strict";
    angular
        .module("pressford.common.services")
        .factory("likeArticleResource", ["$resource", "appsettings", likeArticleResource]);

    function likeArticleResource($resource, appsettings) {
        return $resource(appsettings.serverPath + "/api/articles/like/:articleId", { articleId: "@id" });
    }
}());
