﻿(function() {
    "use strict";
    angular
        .module("pressford.common.services", ["ngResource"])
        .constant("appsettings",
        {
            serverPath: "http://localhost:1263"
        });
}());