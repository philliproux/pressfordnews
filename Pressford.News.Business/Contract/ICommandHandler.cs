﻿namespace Pressford.News.Business.Contract
{
    public interface ICommandHandler<in TCommand>
    {
        void Handle(TCommand command);
    }
}