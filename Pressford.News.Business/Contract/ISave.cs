﻿namespace Pressford.News.Business.Contract
{
    public interface ISave<in TEntity> where TEntity : class
    {
        void Save(TEntity entity);
    }
}