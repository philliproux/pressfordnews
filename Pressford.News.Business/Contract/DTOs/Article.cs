﻿using System;

namespace Pressford.News.Business.Contract.DTOs
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishDate { get; set; }
        public int LikeCount { get; set; }
    }
}
