﻿namespace Pressford.News.Business.Queries
{
    public interface IArticleListQuery
    {
        
    }
    public class PublisherArticlesListQuery : ArticlesListQuery
    {
        public string Username { get; set; }
        public PublisherArticlesListQuery(string username)
        {
            Username = username;
        }
    }
}