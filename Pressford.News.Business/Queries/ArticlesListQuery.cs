﻿using Pressford.News.Business.Contract;
using Pressford.News.Business.Contract.DTOs;
using System.Collections.Generic;

namespace Pressford.News.Business.Queries
{
    public class ArticlesListQuery : IQuery<IEnumerable<Article>>
    {
        public ArticlesListQuery()
        {
            
        }

        public ArticlesListQuery(string username)
        {
            Username = username;
        }
        public string Username { get; private set; }
    }
}
