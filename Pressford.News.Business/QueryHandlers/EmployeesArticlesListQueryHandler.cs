﻿using Pressford.News.Business.Contract;
using Pressford.News.Business.Queries;
using Pressford.News.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using Article = Pressford.News.Business.Contract.DTOs.Article;

namespace Pressford.News.Business.QueryHandlers
{
    public class EmployeesArticlesListQueryHandler : IQueryHandler<ArticlesListQuery, IEnumerable<Article>>
    {
        private readonly IArticleRepository _articleRepository;

        public EmployeesArticlesListQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> Handle(ArticlesListQuery query)
        {
            return _articleRepository
                .GetAllSortedByLatestPublishDate()
                .Select(Mappers.Mappers.MapArticleToArticleDto)
                .ToList();
        }
    }
}