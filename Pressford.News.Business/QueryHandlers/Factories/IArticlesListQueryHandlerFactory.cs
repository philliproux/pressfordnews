﻿using Pressford.News.Business.Contract;
using Pressford.News.Business.Contract.DTOs;
using Pressford.News.Business.Queries;
using System.Collections.Generic;
using System.Security.Principal;

namespace Pressford.News.Business.QueryHandlers.Factories
{
    public interface IArticlesListQueryHandlerFactory
    {
        IQueryHandler<ArticlesListQuery, IEnumerable<Article>> CreateQueryHandler(IPrincipal principal);
    }
}