﻿using Pressford.News.Business.Contract;
using Pressford.News.Business.Queries;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using Article = Pressford.News.Business.Contract.DTOs.Article;

namespace Pressford.News.Business.QueryHandlers.Factories
{
    public class ArticlesListQueryHandlerFactory : IArticlesListQueryHandlerFactory
    {
        private readonly IArticleRepository _articleRepository;

        public ArticlesListQueryHandlerFactory(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IQueryHandler<ArticlesListQuery, IEnumerable<Article>> CreateQueryHandler(IPrincipal principal)
        {
            
            if (principal.IsInRole(UserRoles.Administrator) || principal.IsInRole(UserRoles.Employee))
                return new EmployeesArticlesListQueryHandler(_articleRepository);
            else if (principal.IsInRole(UserRoles.Publisher))
                return new PublisherArticlesListQueryHandler(_articleRepository);
            else
                throw new NotImplementedException("Handle articles for new role");
        }
    }
}