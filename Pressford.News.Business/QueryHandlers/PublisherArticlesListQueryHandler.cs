﻿using Pressford.News.Business.Contract;
using Pressford.News.Business.Contract.DTOs;
using Pressford.News.Business.Queries;
using Pressford.News.Data.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Pressford.News.Business.QueryHandlers
{
    public class PublisherArticlesListQueryHandler : IQueryHandler<ArticlesListQuery, IEnumerable<Article>>
    {
        private readonly IArticleRepository _articleRepository;

        public PublisherArticlesListQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> Handle(ArticlesListQuery query)
        {
            return _articleRepository
                .GetByUsernameSortedByLatestPublishDate(query.Username)
                .Select(Mappers.Mappers.MapArticleToArticleDto)
                .ToList();
        }
    }
}