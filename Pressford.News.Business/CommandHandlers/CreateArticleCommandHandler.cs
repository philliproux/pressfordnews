using Pressford.News.Business.Commands;
using Pressford.News.Business.Contract;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.Entities;
using System;
using Article = Pressford.News.Business.Contract.DTOs.Article;
using DomainArticle = Pressford.News.DomainModel.Entities.Article;

namespace Pressford.News.Business.CommandHandlers
{
    public class CreateArticleCommandHandler : ICommandHandler<CreateArticleCommand>, ISave<DomainArticle>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IArticleRepository _articleRepository;
        private readonly IUserRepository _userRepository;

        public CreateArticleCommandHandler(
            IUnitOfWork unitOfWork, 
            IArticleRepository articleRepository,
            IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _articleRepository = articleRepository;
            _userRepository = userRepository;
        }

        public void Handle(CreateArticleCommand command)
        {
            var publisher = GetPublisher(command.UserName);
            var article = CreateArticle(command.Title, command.Body, command.Author, publisher);
            Save(article);
            command.Article = new Article()
            {
                Id = article.ArticleId,
                Author = article.Author,
                Body = article.Body,
                PublishDate = article.PublishDate,
                Publisher = article.Publisher.DisplayName,
                Title = article.Title
            };
        }

        private ApplicationUser GetPublisher(string username)
        {
            return _userRepository.GetByUsername(username);
        }

        private DomainArticle CreateArticle(string title, string body, string author, ApplicationUser publisher)
        {
            return new DomainArticle()
            {
                Title = title,
                Body = body,
                Author = author,
                Publisher = publisher,
                PublishDate = DateTime.Now // Assumption made here - no Date UI control will be provided
            };
        }

        public void Save(DomainArticle article)
        {
            _articleRepository.Insert(article);
            _unitOfWork.Save();
        }
    }
}