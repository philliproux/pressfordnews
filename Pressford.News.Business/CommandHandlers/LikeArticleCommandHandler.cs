﻿using Pressford.News.Business.Commands;
using Pressford.News.Business.Contract;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.Entities;
using System;

namespace Pressford.News.Business.CommandHandlers
{
    public class LikeArticleCommandHandler : ICommandHandler<LikeArticleCommand>, ISave<Like>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILikeRepository _likeRepository;
        private readonly IUserRepository _userRepository;

        public LikeArticleCommandHandler(
            IUnitOfWork unitOfWork,
            ILikeRepository likeRepository,
            IUserRepository userRepository)
        {
            _unitOfWork = unitOfWork;
            _likeRepository = likeRepository;
            _userRepository = userRepository;
        }

        public void Handle(LikeArticleCommand command)
        {
            var employee = _userRepository.GetByUsername(command.Username);
            var like = new Like()
            {
                ArticleId = command.ArticleId,
                Employee = employee,
                LikedAtDateTime = DateTime.Now,
            };
            Save(like);
        }

        public void Save(Like like)
        {
            _likeRepository.Insert(like);
            _unitOfWork.Save();
        }
    }
}