﻿using Pressford.News.Business.Commands;
using Pressford.News.Business.Contract;
using Pressford.News.Business.Contract.DTOs;
using Pressford.News.Business.Queries;
using Pressford.News.Business.QueryHandlers.Factories;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.DTOs;
using System.Collections.Generic;
using System.Security.Principal;

namespace Pressford.News.Business.Services
{
    public class ArticleService : IArticleService
    {
        private readonly ICommandHandler<CreateArticleCommand> _createArticleCommandHandler;
        private readonly IArticlesListQueryHandlerFactory _articlesListQueryHandlerFactory;
        private readonly ICommandHandler<LikeArticleCommand> _likeArticleCommandHandler;
        private readonly IArticleRepository _articleRepository;

        public ArticleService(
            ICommandHandler<CreateArticleCommand> createArticleCommandHandler,
            IArticlesListQueryHandlerFactory articlesListQueryHandlerFactory,
            ICommandHandler<LikeArticleCommand> likeArticleCommandHandler,
            IArticleRepository articleRepository)
        {
            _createArticleCommandHandler = createArticleCommandHandler;
            _articlesListQueryHandlerFactory = articlesListQueryHandlerFactory;
            _likeArticleCommandHandler = likeArticleCommandHandler;
            _articleRepository = articleRepository;
        }

        public Article Add(string title, string body, string author, string userName)
        {
            var createArticleCommand = new CreateArticleCommand(title, body, author, userName);
            _createArticleCommandHandler.Handle(createArticleCommand);
            return createArticleCommand.Article;
        }

        public IEnumerable<Article> GetArticles(IPrincipal principal)
        {
            var queryHandler = _articlesListQueryHandlerFactory.CreateQueryHandler(principal);
            return queryHandler.Handle(new ArticlesListQuery(principal.Identity.Name));
        }

        public void LikeArticle(IPrincipal principal, int articleId)
        {
            var likeArticleCommand = new LikeArticleCommand(articleId, principal.Identity.Name);
            _likeArticleCommandHandler.Handle(likeArticleCommand);
        }

        // TODO: Can refactor into QueryHandler
        public IEnumerable<ArticleLiked> GetLikesForLastThirtyDaysGroupedByArticleId(int articleId)
        {
            return _articleRepository.GetLikesForLastThirtyDaysGroupedByArticleId(articleId);
        }
    }
}
