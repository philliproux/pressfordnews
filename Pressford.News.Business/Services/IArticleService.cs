using Pressford.News.Business.Contract.DTOs;
using Pressford.News.DomainModel.DTOs;
using System.Collections.Generic;
using System.Security.Principal;

namespace Pressford.News.Business.Services
{
    public interface IArticleService
    {
        Article Add(string title, string body, string author, string userName);
        IEnumerable<Article> GetArticles(IPrincipal principal);
        void LikeArticle(IPrincipal principal, int articleId);
        IEnumerable<ArticleLiked> GetLikesForLastThirtyDaysGroupedByArticleId(int articleId);
    }
}