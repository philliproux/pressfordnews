﻿namespace Pressford.News.Business.Commands
{
    public class LikeArticleCommand
    {
        public LikeArticleCommand(int articleId, string username)
        {
            ArticleId = articleId;
            Username = username;
        }

        public int ArticleId { get; private set; }
        public string Username { get; private set; }
    }
}
