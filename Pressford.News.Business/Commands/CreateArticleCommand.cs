﻿using Pressford.News.Business.Contract.DTOs;

namespace Pressford.News.Business.Commands
{
    public class CreateArticleCommand
    {
        public string Title { get; private set; }
        public string Body { get; private set; }
        public string Author { get; private set; }
        public string UserName { get; private set; }
        public Article Article { get; set; } /* Output Property */

        public CreateArticleCommand(string title, string body, string author, string userName)
        {
            this.Title = title;
            this.Body = body;
            this.Author = author;
            this.UserName = userName;
        }
    }
}