using Pressford.News.Business.Contract.DTOs;

namespace Pressford.News.Business.Mappers
{
    public static class Mappers
    {
        public static Article MapArticleToArticleDto(DomainModel.Entities.Article article)
        {
            return new Article()
            {
                Id = article.ArticleId,
                Author = article.Author,
                Body = article.Body,
                Publisher = article.Publisher.DisplayName,
                Title = article.Title,
                PublishDate = article.PublishDate,
                LikeCount = article.Likes.Count
            };
        }
    }
}