﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Business.Queries;
using Pressford.News.Business.QueryHandlers;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pressford.News.Business.Tests.QueryHandlers
{
    public class EmployeesArticlesListQueryHandlerTests
    {
        [Test]
        public void QueryHandlerYieldsResultsFromDataStore()
        {
            var articleRepository = A.Fake<IArticleRepository>();
            var sut = new EmployeesArticlesListQueryHandler(articleRepository);
            var query = new ArticlesListQuery();
            var publishDate = DateTime.Now;
            var articles = new List<Article>()
            {
                new Article()
                {
                    Title = "Title1",
                    Author = "Author1",
                    Body = "Body1",
                    Publisher = new ApplicationUser() { FirstName = "First1", LastName = "Last1"},
                    PublishDate = publishDate,
                    Likes = new List<Like>()
                },
                new Article()
                {
                    Title = "Title2",
                    Author = "Author2",
                    Body = "Body2",
                    Publisher = new ApplicationUser() { FirstName = "First2", LastName = "Last2"},
                    PublishDate = publishDate,
                    Likes = new List<Like>()
                }
            };
            A.CallTo(() => articleRepository.GetAllSortedByLatestPublishDate()).Returns(articles);

            var articleDtos = sut.Handle(query).ToArray();

            Assert.IsInstanceOf<IEnumerable<Contract.DTOs.Article>>(articleDtos);
            Assert.That(2, Is.EqualTo(articleDtos.Count()));
            Assert.That("Title1", Is.EqualTo(articleDtos.First().Title));
            Assert.That("Author1", Is.EqualTo(articleDtos.First().Author));
            Assert.That("Body1", Is.EqualTo(articleDtos.First().Body));
            Assert.That("First1 Last1", Is.EqualTo(articleDtos.First().Publisher));
            Assert.That(publishDate, Is.EqualTo(articleDtos.First().PublishDate));
        }
    }
}
