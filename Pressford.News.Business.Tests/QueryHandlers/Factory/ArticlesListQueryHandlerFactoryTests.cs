﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Business.QueryHandlers;
using Pressford.News.Business.QueryHandlers.Factories;
using Pressford.News.Data.Repositories;
using System.Security.Claims;
using System.Security.Principal;

namespace Pressford.News.Business.Tests.QueryHandlers.Factory
{
    public class ArticlesListQueryHandlerFactoryTests
    {
        private IArticleRepository _articleRepository;
        [SetUp]
        public void Setup()
        {
            _articleRepository = A.Fake<IArticleRepository>();
        }

        [Test]
        public void EmployeesArticlesListQueryHandlerInstanceCreatedForAdministrators()
        {
            var sut = new ArticlesListQueryHandlerFactory(_articleRepository);
            var principal = new GenericPrincipal(new ClaimsIdentity(), new[] {"Administrator"});
            
            var queryHandler = sut.CreateQueryHandler(principal);

            Assert.IsInstanceOf<EmployeesArticlesListQueryHandler>(queryHandler);
        }

        [Test]
        public void EmployeesArticlesListQueryHandlerInstanceCreatedForEmployees()
        {
            var sut = new ArticlesListQueryHandlerFactory(_articleRepository);
            var principal = new GenericPrincipal(new ClaimsIdentity(), new[] { "Employee" });

            var queryHandler = sut.CreateQueryHandler(principal);

            Assert.IsInstanceOf<EmployeesArticlesListQueryHandler>(queryHandler);
        }

        [Test]
        public void PublisherArticlesListQueryHandlerInstanceCreatedForEmployees()
        {
            var sut = new ArticlesListQueryHandlerFactory(_articleRepository);
            var principal = new GenericPrincipal(new ClaimsIdentity(), new[] { "Publisher" });

            var queryHandler = sut.CreateQueryHandler(principal);

            Assert.IsInstanceOf<PublisherArticlesListQueryHandler>(queryHandler);
        }
    }
}
