﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Business.CommandHandlers;
using Pressford.News.Business.Commands;
using Pressford.News.Data.Instracture;
using Pressford.News.Data.Repositories;
using Pressford.News.DomainModel.Entities;
using System;

namespace Pressford.News.Business.Tests.Handlers
{
    public class CreateArticleCommandHandlerTests
    {
        private IUnitOfWork _unitOfWork;
        private IArticleRepository _articleRepository;
        private IUserRepository _userRepository;

        [SetUp]
        public void Setup()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _articleRepository = A.Fake<IArticleRepository>();
            _userRepository = A.Fake<IUserRepository>();
        }

        [Test]
        public void ArticleCreatedFromCommandAndPropertiesSet()
        {
            var sut = new CreateArticleCommandHandler(_unitOfWork, _articleRepository, _userRepository);
            var command = new CreateArticleCommand("Title of book", "Body of article", "Author name", "test@abcpublising.com");
            A.CallTo(() => _userRepository.GetByUsername("test@abcpublising.com")).Returns(new ApplicationUser() { FirstName = "John", LastName = "Deer"});

            sut.Handle(command);
            Assert.IsInstanceOf<Contract.DTOs.Article>(command.Article);
            Assert.AreEqual("Title of book", command.Article.Title);
            Assert.AreEqual("Body of article", command.Article.Body);
            Assert.AreEqual("Author name", command.Article.Author);
            Assert.AreEqual("John Deer", command.Article.Publisher);
        }

        [Test]
        public void PublishedDateSet()
        {
            var sut = new CreateArticleCommandHandler(_unitOfWork, _articleRepository, _userRepository);
            var command = new CreateArticleCommand("Title of book", "Body of article", "Author name", "test@abcpublising.com");
            sut.Handle(command);
            Assert.IsTrue(DateTime.Now.AddSeconds(-2) <= command.Article.PublishDate && command.Article.PublishDate <= DateTime.Now);
        }

        [Test]
        public void ArticleAddedToDatastore()
        {
            var sut =  new CreateArticleCommandHandler(_unitOfWork, _articleRepository, _userRepository);
            var command = new CreateArticleCommand("Title of book", "Body of article", "Author name", "test@abcpublising.com");
            using (var scope = Fake.CreateScope())
            {
                sut.Handle(command);

                using (scope.OrderedAssertions())
                {
                    A.CallTo(() => _articleRepository.Insert(A<Article>.Ignored)).MustHaveHappened();
                    A.CallTo(() => _unitOfWork.Save()).MustHaveHappened();
                }
            }
        }
    }
}
