﻿using FakeItEasy;
using NUnit.Framework;
using Pressford.News.Business.Commands;
using Pressford.News.Business.Contract;
using Pressford.News.Business.QueryHandlers.Factories;
using Pressford.News.Business.Services;
using Pressford.News.Data.Repositories;

namespace Pressford.News.Business.Tests.Services
{
    public class ArticleServiceTests
    {
        private ICommandHandler<CreateArticleCommand> _createCommandHandler;
        private IArticlesListQueryHandlerFactory _articlesListQueryHandlerFactory;
        private ICommandHandler<LikeArticleCommand> _likeArticleCommandHandler;
        private IArticleRepository _articleRepository;

        [SetUp]
        public void Setup()
        {
            _createCommandHandler = A.Fake<ICommandHandler<CreateArticleCommand>>();
            _articlesListQueryHandlerFactory = A.Fake<IArticlesListQueryHandlerFactory>();
            _likeArticleCommandHandler = A.Fake<ICommandHandler<LikeArticleCommand>>();
            _articleRepository = A.Fake<IArticleRepository>();
        }
        
        [Test]
        public void AddArticleHandledByCommandHandler()
        {
            var sut = new ArticleService(_createCommandHandler, _articlesListQueryHandlerFactory, _likeArticleCommandHandler, _articleRepository);
            sut.Add("Title of book", "Body of article", "Author name", "test@abcpublising.com");
            
            A.CallTo(() => _createCommandHandler.Handle(A<CreateArticleCommand>.That.Matches(x => x.Title == "Title of book"))).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}
